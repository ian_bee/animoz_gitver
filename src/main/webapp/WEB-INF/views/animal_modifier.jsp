<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Animoz - <c:out value="${animal.nom}"/> - Modification</title>
</head>
<body>
	<nav>
		<a href="<c:url value='/'/>">Accueil</a>
		<a href="<c:url value='/animal'/>">Liste des animaux</a>
	</nav>
	<h1>Modification de <c:out value="${animal.nom}"/> *</h1>
	<%-- 
	<section>
		<h2>Origine</h2>
		<p><c:out value="${animal.origine}"/></p>
	</section>
	<section>
		<h2>Espèce</h2>
		<p><c:out value="${animal.espece.nom}"/></p>
	</section>
	<section>
		<h2>Description</h2>
		<p><c:out value="${animal.description}"/></p>
	</section>
	<section>
		<h2>Infos diverses</h2>
		<p>Régime&nbsp;: <c:out value="${animal.regime}"/></p>
		<p>Population totale sur le site&nbsp;: <c:out value="${animal.populationTotale}"/></p>
	</section>
	--%>
	
	<form:form servletRelativeAction="/animal" modelAttribute="animalDto">
				
				<form:input path="nom" value="${animal.nom}"/>
				
				<form:select path="espece.id">
				<form:options items="${especes}" itemLabel="nom" itemValue="id"  />
				</form:select>
								
				<form:input path="origine" value="${animal.origine}"/>
				
				<form:select path="regime">
				<form:options items="${regimes}"/>
				</form:select>
				<br>
				<form:textarea path="description" value="${animal.description}"/>
				
				
				<button type="submit">Modifier</button>
				<%--<form:errors path="nom"/>--%>
			</form:form>
	
	
	
</body>
</html>