<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Animoz - Liste des animaux</title>
</head>
<body>
	<nav>
		<a href="<c:url value='/'/>">Accueil</a>
	</nav>
	<ul>
		<c:forEach items="${animaux}" var="animal">
			<li>
			<a href="<c:url value='/animal/${animal.id}'/>"><c:out value="${animal.nom}"/></a>
			
			</li>
		</c:forEach>
		
		<!-- AJOUTER UN ANIMAL -->
	</ul>
		
		
			<form:form servletRelativeAction="/animal" modelAttribute="animalDto">
				
				<form:input path="nom"/>
				
				<form:select path="espece.id">
				<form:options items="${especes}" itemLabel="nom" itemValue="id"/>
				</form:select>
								
				<form:input path="origine"/>
				
				<form:select path="regime">
				<form:options items="${regimes}"/>
				</form:select>
				<br>
				<form:textarea path="description"/>
				
				
				<button type="submit">Ajouter</button>
				<%--<form:errors path="nom"/>--%>
			</form:form>
		
		
		
	
	
	<!-- SUPPRIMER UN ANIMAL -->
	
	<form:form servletRelativeAction="/animal/disparition" method="post" modelAttribute="animalDto">
		<form:select path="nom">
			<form:options items="${animaux}" itemLabel="nom" itemValue="nom"/>
		</form:select>
		<button type="submit">Supprimer Animal</button>
	</form:form>
	
	
</body>
</html>