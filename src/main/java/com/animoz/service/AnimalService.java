package com.animoz.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.dao.AnimalDao;
import com.animoz.modele.Animal;
import com.animoz.modele.Espece;
import com.animoz.modele.Regime;

@Service
public class AnimalService {
	
	@Autowired
	private AnimalDao animalDao;

	public List<Animal> getAnimaux() {
		return animalDao.getAnimaux();
	}

	public Animal getAnimal(long animalId) {
		return animalDao.getAnimal(animalId);
	}

	@Transactional
	public void ajouter(String nom, String origine, String description, Regime regime, Espece espece) {
		if(! animalDao.existe(nom)) {
			Animal animal = new Animal();
			animal.setNom(nom);
			animal.setEspece(espece);
			animal.setDescription(description);
			animal.setOrigine(origine);
			animal.setRegime(regime);
			animalDao.ajouter(animal);
			
			// // Rajouter une validation de l'espèce (pour l'instant juste limitée par la liste déroulante)
			
		}
		
	}
	
	@Transactional
	public void supprimer(String nom) {
		animalDao.supprimer(nom);
		
	}

}
