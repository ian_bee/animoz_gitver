package com.animoz.controleur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.animoz.modele.Regime;
import com.animoz.service.AnimalService;
import com.animoz.service.EspeceService;

@Controller
public class AnimalControleur {
	
	@Autowired
	private AnimalService animalService;
	@Autowired
	private EspeceService especeService;
	
	@GetMapping("/animal")
	public String getListeAnimaux(Model model, @ModelAttribute AnimalDto animalDto) {
		model.addAttribute("animaux", animalService.getAnimaux());
		model.addAttribute("especes", especeService.getEspeces());
		model.addAttribute("regimes", Regime.values());
		//Comme ça on peut faire une liste déroulante avec les espèces à sélectionner
		return "animaux";
	}

	@GetMapping("/animal/{animalId}")
	public String getAnimal(Model model, @PathVariable long animalId) {
		model.addAttribute("animal", animalService.getAnimal(animalId));
		return "animal";
	}
	

	
	
	// // // // // // // //    MODIFIER - AJOUTER - SUPPRIMER    // // // // // // // /
	
	
	@GetMapping("/animal/{animalId}/modifier")
	public String getAnimalModifier(Model model, @PathVariable long animalId, @Valid @ModelAttribute AnimalDto animalDto, BindingResult binding) {
		model.addAttribute("animal", animalService.getAnimal(animalId));
		model.addAttribute("especes", especeService.getEspeces());
		model.addAttribute("regimes", Regime.values());
		return "animal_modifier";
	}
	
	
	@PostMapping("/animal")
	public String ajouterAnimal(Model model, @Valid @ModelAttribute AnimalDto animalDto, BindingResult binding) {
		
		if(binding.hasErrors()) {
			return getListeAnimaux(model, animalDto);
		}
				animalService.ajouter(animalDto.getNom(), animalDto.getOrigine(), animalDto.getDescription(), animalDto.getRegime(), animalDto.getEspece());
				return "redirect:/animal";
	}
	
	
	
	@PostMapping("/animal/disparition")
	public String supprimerAnimal(Model model, @ModelAttribute AnimalDto animalDto){
		animalService.supprimer(animalDto.getNom());
		return "redirect:/animal"; // Animal est la liste d'animaux !
	}
	
	
	
}
