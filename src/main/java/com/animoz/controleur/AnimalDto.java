package com.animoz.controleur;


import org.hibernate.validator.constraints.NotBlank;

import com.animoz.modele.Espece;
import com.animoz.modele.Regime;

public class AnimalDto {

	@NotBlank(message = "Vous devez fournir un nom pour l''animal !")
	private String nom;
		
	private Espece espece;
	private String origine;
	private String description;
	
	private Regime regime;
	
	// // // GET SET // // /
	
	public Regime getRegime() {
		return regime;
	}

	public void setRegime(Regime regime) {
		this.regime = regime;
	}

	public String getOrigine() {
		return origine;
	}

	public void setOrigine(String origine) {
		this.origine = origine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Espece getEspece() {
		return espece;
	}

	public void setEspece(Espece espece) {
		this.espece = espece;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	

}
